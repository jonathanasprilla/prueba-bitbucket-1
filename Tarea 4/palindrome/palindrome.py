from sys import stdin

def solve(word, N):
    i, j = 0, N - 1
    diff = 0
    while i < j:
        if word[i] == word[j]:
            i += 1;j -= 1
        else:
            diff += 1;i += 1
    return diff

def showResult(case, K, diff):
    if diff < 1:
        print("Case {0}: {1}".format(case + 1, "Too easy"))
    elif diff > K:
        print("Case {0}: {1}".format(case + 1, "Too difficult"))
    else:
        print("Case {0}: {1}".format(case + 1, diff))

def main():
    T = int(stdin.readline())
    for i in range(T):
        N, K = map(int, stdin.readline().split())
        word = stdin.readline().split()
        diff1 = solve(word, N)
        word.reverse()
        diff2 = solve(word, N)
        showResult(i, K, min(diff1, diff2))
main()