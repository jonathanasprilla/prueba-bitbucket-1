from sys import stdin

def solve(profit, loss):
    ans = 0
    # Profit for four months, a loss of one month 4 * profit < loss , then at least two months of loss in a year, so the final profit is 10*profit-2*loss 
    if loss > 4 * profit:
        ans = 10 * profit - 2 * loss
    # Three months of profit, two months of loss 3*profit <2*loss then there must be at least four months of loss in a year, so the final profit is 8*profit-4*loss
    elif 2 * loss > 3 * profit:
        ans = 8 * profit - 4 * loss
    # Two months of profit, three months of loss 2*profit <3*loss then there must be at least six months of loss in a year, so the final profit is 6*profit-6*loss  
    elif 3 * loss > 2 * profit:
        ans = 6 * profit - 6 * loss
    # Profit in one month, loss in four months profit <4*loss Then there must be at least nine months of loss in a year, so the final profit is 3*profit-9*loss
    elif 4*loss > profit:
        ans = 3 * profit - 9 * loss
    # Five months of total loss, then every month of the year will lose money without profit
    else:
        ans = -1
    return ans

def veredict(ans):
    if ans >= 0:
        print(ans)
    else:
        print("Deficit")

def main():
    case = stdin.readline().split()
    while  len(case)>0:
        s,d = int(case[0]),int(case[1])
        veredict(solve(s,d))
        case = stdin.readline().split()

main()