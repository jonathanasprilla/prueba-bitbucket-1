"""

Jonathan Asprilla Saavedra

Código: 8926036

Código de Honor
Como miembro de la comunidad académica de la Pontificia Universidad Javeriana Cali, los valores éticos y la
integridad son tan importantes como la excelencia académica. En este curso se espera que los estudiantes se
comporten ética y honestamente, con los más altos niveles de integridad escolar. En particular, se asume que
cada estudiante adopta el siguiente código de honor:
Como miembro de la comunidad académica de la Pontificia Universidad Javeriana Cali me comprometo a
seguir los más altos estándares de integridad académica.
Integridad académica se refiere a ser honesto, dar crédito a quien lo merece y respetar el trabajo de los
demás. 

"""


from sys import stdin
import math
a1,a2,y,b1,b2,s,end,E,G = None,None,None,None,None,None,None,None,None
INF = float('inf') # valor infinito positivo para calcular el minimo
p1 = [-1,0,1, 0] 
p2 = [ 0,1,0,-1]
#p1 y p2 son listas para calcular los adyacentes de un bloque de una manera mas sencilla, donde p1 son las posiciones
#a sumar de "y" y p2 de "x"

def calc(h1,h2):
	"""
	h1= altura del bloque actual
	h2 =altura del bloque adyacente
	Funcion que calcula el tiempo y la energia gastada de ir de un bloque de altura h1 a uno h2 teniendo en cuenta
	las constantes y condiciones dadas en el enunciado y usando la funcion ceil para aproximar hacia arriba los valores flotantes
	de realizar las operaciones. La salida del tiempo y la energia se devuelve con una lista de 2 posiciones donde la primera posicion
	represan la energia y la segunda el tiempo
	"""
	global a1,a2,y,b1,b2,s
	energy ,time = 0,0
	if( h1>h2 ):
		energy = math.ceil(a1*(h1-h2)) + y
		time = math.ceil(b1*(h1-h2)) + s
	elif(h1 == h2):
		energy =  y
		time =  s
	else:
		energy = math.ceil(a2*(h2-h1)) + y
		time = math.ceil(b2*(h2-h1)) + y

	return [energy,time]

def solve(posy,posx,e,visited,N,M):
	"""
	posx posy = posiciones actual en la matriz respectivamente
	e = energia gastada actualmente
	visited = matriz de visitados
	N,M = tamaño de la matriz
	Funcion que calcula el tiempo minimo de ir de una casilla a una meta, teniendo en cuenta la posicion actual en la matriz,
	el objetivo final y la energia gastada. Tambien se tiene en cuenta que una vez la casilla sea visitada no se puede volver a pasar
	por esa misma en el mismo camino y la energia nunca se pase del limite que se puede gastar como poda. Ademas para calcular las
	casillas adyacentes se tiene en cuenta que las posiciones no se salgan de la matriz y se recurre hasta llegar a la casilla final y se
	usa la funcion calc para calcular el costo de energia y tiempo de cruzar casillas adyacentes. La salida es un numero flotante que representa
	el tiempo.
	"""
	global a1,a2,y,b1,b2,s,end,E,G,INF,p1,p2
	ans = 0
	
	if(posy == end[0] and posx == end[1]): #se verifica primero si ya se encuentra en la casilla final
		ans = 0 #de ser asi la respuesta es 0
	else:
		tans = INF  #variable temporal para el calculo del minimo tiempo
		for i in range(4): #se recorren las 4 posiciones posibles de casillas adyacentes
			ty,tx = posy + p1[i],posx + p2[i] #ty y tx representa la posicion de una casilla adyacente
			
			if(0 <= ty < N and 0 <= tx < M and visited[ty][tx] == 0): #se verficia que ty y tx no se salga de la matriz
																	  #y que esa casilla no haya sido visitada
				visited[ty][tx] = 1   #se visita la casilla
				h1,h2 = G[posy][posx],G[ty][tx] #se reemplaza los valores de las alturas de la matriz en h1 y h2 respectivamente
				energy,time = calc(h1,h2)#se llama a la funcion que calcula la energia y el tiempo
				
				if e+energy > E: #si la energia calculada mas la actual se pasa de la maxima que se puede gastar
					tans = INF #la respuesta es infinito
				else:
					tans = min(time + solve(ty,tx,e + energy,visited,N,M),tans)
					#si no se recurre reemplazando la posicion actual por ty,tx , la energia acomulada y la lista de visitados
				visited[ty][tx] = 0 # se vuelve a poner como no visitado la casilla para realizar el backtrack
		
		ans = tans			
	return ans


def main():
	"""
	entrada y salida del problema
	"""
	global a1,a2,y,b1,b2,s,end,E,G
	N,M = map(int,stdin.readline().split())
	while N!= 0 and M != 0:
		a1,a2,y = map(float,stdin.readline().split())
		b1,b2,s = map(float,stdin.readline().split())
		s,y = int(s),int(y)

		G = list() #MAtriz de las alturas

		for _ in range(M):
			G.append(list(map(int,stdin.readline().split())))
			
		line = list(map(int,stdin.readline().split()))
		start,end,E = [line[0]-1,line[1]-1],[line[2]-1,line[3]-1],line[4] #posiciones de comienzo, final y cantidad de energia maxima a gastar
		
		visited = [[0 for _ in range(N)] for _ in range(M)] #lista de visitados
		visited[start[0]][start[1]] = 1 #se pone como visitado la posicion inicial
		
		ans = solve(start[0],start[1],0,visited,N,M) #se llama a la funcion solucion
		if(ans == INF): #si el resultado es infinito significa que no se puede llegar al final con las restricciones dadas
			print("failed")
		else:
			print(ans)

		N,M = map(int,stdin.readline().split())
main()