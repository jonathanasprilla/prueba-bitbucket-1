#Jonathan Asprilla Saavedra
from sys import stdin

def trib(n, back):
    if n <= 1:
        return 1
    if tab[n][back] != 0:
        return tab[n][back]
    tab[n][back] = 1
    for i in range(1, back+1):
        tab[n][back] += trib(n-i, back)
    return tab[n][back]

def main():
    global tab
    tab = [ [0 for _ in range(61)] for _ in range(61)]
    entry = stdin.readline().split()
    case=1
    while entry:
        n = int(entry[0]); back = int(entry[1])
        if n < 61:
            print("Case {}: {}".format(case, trib(n, back)))
        entry = stdin.readline().split()
        case+=1

main()