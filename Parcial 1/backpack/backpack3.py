from sys import stdin

INF = float("inf")


def Solve(x):
	y = x
	camp = 0
	for i in range(0,Camps+1):
		dif = acum[i+1] - acum[i]
		if y >= dif:
			y-=dif
		else:
			camp+=1
			y = x-dif
		if camp > Nights or y < 0:
			return False
	return True


def main():
    global Camps, Nights, Points, acum
    tokens = list(map(int,(stdin.readline().split())))
    Points = []
    while len(tokens) != 0:
        Camps, Nights = tokens[0], tokens[1]
        acum = [0 for _ in range(Camps+2)]

        Points.append(0)
        for i in range(1,Camps+2):
            Points.append(int(stdin.readline()))

        acum[0] = 0
        for i in range(1,Camps+2):
            acum[i] = acum[i-1] + Points[i]

        low,high = acum[0], acum[Camps+1]
        dist = INF
        while low <= high:
            mid = low + ((high-low)>>1)
            if Solve(mid):
                dist = min(dist,mid)
                high = mid - 1
            else:
                low = mid + 1

        print(low)
        tokens = list(map(int,(stdin.readline().split())))
main()