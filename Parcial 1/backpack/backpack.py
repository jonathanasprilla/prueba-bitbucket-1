#Jonathan Asprilla Saavedra
from sys import stdin

def Solve2(x):
	Sum, cont = 0,0
	for i in range(Camps+1):
		if Points[i] > x:
			return 0
		if Sum + Points[i] > x:
			Sum = Points[i]
			cont += 1
		else:
			Sum += Points[i]
	return cont <= Nights

def Solve():
	low = 0
	high = 1000000
	while low+1 != high:
		mid = low + ((high-low)>>1)
		if(Solve2(mid)):
			high = mid
		else:
			low = mid
	return high

def main():
	global Camps,Nights,Points
	tokens = list(map(int,(stdin.readline().split())))
	Points = []
	while len(tokens) != 0:
		Camps, Nights = tokens[0], tokens[1]
		for _ in range(0,Camps+1):
			Points.append(int(stdin.readline()))
		print(Solve())
		Points = []
		tokens = list(map(int,(stdin.readline().split())))
main()