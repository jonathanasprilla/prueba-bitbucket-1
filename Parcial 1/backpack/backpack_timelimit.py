#Jonathan Asprilla Saavedra
from sys import stdin
from sys import setrecursionlimit
setrecursionlimit(1<<24)

INF= float('inf')

def cicleMid(dim,c,k): 
    arr = [];acu = 0;n = len(c)
    ans = 1
    i = 0
    while i < n :
        if c[i] > dim:
            ans = 2
            i = n
        elif (acu + c[i]) > dim:
            k=k-1
            arr.append(acu)
            acu = 0
            i = i-1
        else:
            acu += c[i]
        i += 1
    if (k < 0):
        ans = 2
    arr.append(acu)
    return ans,arr

def bis(minV,maxV,camps,k):
    ans = None
    arr2 = []
    if k == 0:
        ans = sum(camps)
    elif (k >= len(camps)-1):
        ans = max(camps)
    else:
        while minV +1 < maxV:  
            mid = (minV + maxV)>>1
            bu,arr = cicleMid(mid,camps,k)
            if bu == 1:
                maxV = mid 
                arr2.append(max(arr))
            elif bu == 2:
                minV = mid
        ans = min(arr2)
    return ans

def main():
    readT = 1
    while (readT == 1):
        inputT = stdin.readline().split()
        if (inputT == []):
            readT = 0
        else:
            N,K = int(inputT[0]),int(inputT[1])
            camps = []
            for i in range(N+1):
                camps.append(int(stdin.readline()))
            print(bis(0,sum(camps),camps,K))

main()