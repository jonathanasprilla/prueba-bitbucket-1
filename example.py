def sum(a, b):
	return a + b

def multiply(a, b):
	return a * b


def main():
	flag = 1
	while flag:
		a = int(input("type a number to sum: "))
		b = int(input("type another number to sum: "))
		print(" {0} + {1} = {2}.".format(a,b,sum(a,b)))
		print(" {0} x {1} = {2}.".format(a,b,multiply(a,b)))
		flag = int(input("continue? type 1 for yes or 0 for no: "))
		print("\n")
main()